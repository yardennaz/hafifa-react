type pointsMultiplier = {
  start: number;
  end: number;
  multiplier: number;
};

const pointsMultiplier: pointsMultiplier[] = [
  { start: 0, end: 0.2, multiplier: 1 },
  { start: 0.2, end: 0.3, multiplier: 0.75 },
  { start: 0.3, end: 0.35, multiplier: 0.5 },
  { start: 0.35, end: 0.4, multiplier: 0.25 },
];

const timeMultiplier = () => {
    const timePointsCalculator = (TimePart: number) => {
    const POINTS_PER_QUESTION = 800;
    const TIME_FULL_POINTS = 200;

    const timeMultiplier: pointsMultiplier | undefined = pointsMultiplier.find(
      (part) => TimePart > part.start && TimePart < part.end
    );

    return (
      POINTS_PER_QUESTION +
      TIME_FULL_POINTS * (timeMultiplier ? timeMultiplier.multiplier : 0)
    );
  };

  return { timePointsCalculator };
};

export { timeMultiplier };