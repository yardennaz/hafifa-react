import { useContext, useCallback, FC, useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";

import { props } from "./types";
import { answer } from "../../types";

import QuestionTimer from "../../components/timer";
import { QuizContext } from "../../store/gameStore";
import { timeMultiplier } from "./calcTimeMultiplier";
import GameStyle from "./Game.module.css";
import { shuffle } from "./shuffleArray";

const GameView: FC<props> = ({ pushNewAnswer, timer, questions }) => {
  console.log("<Game View>");

  const navigation = useNavigate();
  const { timePointsCalculator } = timeMultiplier();
  const [startingTime, setStartingTime] = useState<number>(
    new Date().getTime()
  );

  const {
    currentQuestion,
    changeQuestion,
    points,
    setNewPoints,
    emptyPoints,
  } = useContext(QuizContext);

  useEffect(() => {
    changeQuestion(0);
    emptyPoints();
  }, []);

  const selectAnswer = (answer: answer) => {
    pushNewAnswer(answer);

    changeQuestion(currentQuestion + 1);

    if (answer.correctAnswer === answer.userAnswer) {
      setNewPoints(
        timePointsCalculator(
          (new Date().getTime() - startingTime) / (timer * 1000)
        )
      );
    }

    setStartingTime(() => new Date().getTime());
  };

  const onTimer = useCallback(() => {
    selectAnswer({
      userAnswer: "no answer",
      correctAnswer: "",
      question: questions[currentQuestion].question,
    });
  }, [currentQuestion]);

  const isQuizOver = currentQuestion === questions.length;

  useEffect(() => {
    if (questions.length === 0) {
      navigation("/");
    } else if (currentQuestion === questions.length) {
      navigation("/summary");
    }
  }, [currentQuestion]);

  return (
    <div>
      <h1 className="text-white text-center">
        שאלה מספר {currentQuestion + 1}/{questions.length}
      </h1>

      {!isQuizOver && (
        <>
          <div className="row justify-center">
            <QuestionTimer
              time={timer * 1000}
              onTimeout={onTimer}
              key={currentQuestion}
            />
          </div>
          <h2 className="text-center text-white">
            {questions[currentQuestion].question}
          </h2>
          <ul>
            {shuffle<string>([
              ...questions[currentQuestion].incorrectAnswers,
              questions[currentQuestion].correctAnswer,
            ]).map((possibleAnswer: string) => (
              <li key={possibleAnswer} className="row justify-center">
                <button
                  className={`${GameStyle["button-large"]} button blue-background `}
                  onClick={() =>
                    selectAnswer({
                      userAnswer: possibleAnswer,
                      correctAnswer: questions[currentQuestion].correctAnswer,
                      question: questions[currentQuestion].question,
                    })
                  }
                >
                  {possibleAnswer}
                </button>
              </li>
            ))}
          </ul>
          <h2 className="text-center text-red">מצב נקודות: {points}</h2>
        </>
      )}
    </div>
  );
};

export default GameView;
