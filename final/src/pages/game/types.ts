import { answer, question } from "../../types";

type props = {
  pushNewAnswer: (answer: answer) => void;
  answers: answer[];
  timer: number;
  questions: question[];
};

export type { props };
