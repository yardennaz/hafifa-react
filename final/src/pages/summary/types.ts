import { answer } from "../../types"

type props = {
    answers: answer[],
    resetAnswers: () => void,
}

export type { props };