import { Link, useNavigate } from "react-router-dom";
import { FC, useContext, useEffect } from "react";
import { props } from "./types";
import { QuizContext } from "../../store/gameStore";
import MyFireworksComponent from "../../components/fireworks";
import SummaryStyle from "./Summary.module.css";

const SummaryView: FC<props> = ({ answers, resetAnswers }) => {
  console.log("<Summary View>");

  const navigate = useNavigate();
  const { points, changeQuestion } = useContext(QuizContext);

  useEffect(() => {
    if (answers.length === 0) {
      navigate("/");
    }
  }, []);

  return (
    <div className="text-white border">
      {points / (answers.length * 1000) > 0.75 && <MyFireworksComponent />}
      <Link
        to={"/"}
        onClick={() => {
          changeQuestion(0);
          resetAnswers();
        }}
        className={`button ${SummaryStyle["correct-answer"]} text-black ${SummaryStyle["back-to-home-button"]}`}
      >
        חזרה למסך בית
      </Link>

      <h1 className="text-center">סיכום</h1>
      <h2 className="text-center">
        {points}/{answers.length * 1000} :ניקוד סופי
      </h2>
      <div>
        {answers.map((answer) => (
          <div
            key={answer.question}
            className={`${SummaryStyle["half-screen"]} margin-auto`}
          >
            <div className="mt-5 border">
              <h3 className="text-center">{answer.question} :השאלה </h3>
              <h3
                className={`border text-center user-answer 
                                        ${
                                          answer.correctAnswer ===
                                          answer.userAnswer
                                            ? SummaryStyle["correct-answer"]
                                            : SummaryStyle["wrong-answer"]
                                        }`}
              >
                {answer.userAnswer} :התשובה שלך
              </h3>
              <h3 className="text-center">
                {answer.correctAnswer} :התשובה הנכונה{" "}
              </h3>
            </div>
          </div>
        ))}
      </div>
    </div>
  );
};

export default SummaryView;
