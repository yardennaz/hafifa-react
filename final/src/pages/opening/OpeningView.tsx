import { Link } from "react-router-dom";
import { FC } from "react";
import { props } from "./types";
import quiz from "../../assets/qiuz.jpg";
import loader from "../../assets/loader.svg";
import OpeningStyle from "./Opening.module.css";

const OpeningView: FC<props> = ({
  getQuestions,
  setTimer,
  isQuestionsLoading,
}) => {
  console.log("<Opening View>");

  const startGame = () => {
    getQuestions(10, "9", "easy", "multiple");
    setTimer(15);
  };

  return (
    <div>
      <img src={quiz} className={`${OpeningStyle.pic}`} />

      <h1 className="text-white text-center">חידון לידיעת הארץ</h1>

      <div className={`${OpeningStyle.toolbar}`}>
        <button className="button blue-background" onClick={startGame}>
          {!isQuestionsLoading ? (
            "התחל משחק"
          ) : (
            <img src={loader} className={`${OpeningStyle.spinner}`} />
          )}
        </button>
        <Link to="/settings">
          <button className="button blue-background"> משחק מותאם אישית </button>
        </Link>
      </div>
    </div>
  );
};

export default OpeningView;
