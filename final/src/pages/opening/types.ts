export type props = {
  getQuestions: (
    amount: number,
    category: string,
    difficulty: string,
    type: string
  ) => void;
  setTimer: (time: number) => void;
  isQuestionsLoading: boolean;
};
