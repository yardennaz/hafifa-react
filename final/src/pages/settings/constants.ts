import { option } from "../../types";

const difficulties: option[] = [{ text: 'קל', value: 'easy' }, { text: 'בינוני', value: 'medium' }, { text: 'קשה', value: 'hard' }];
const types: option[] = [{ text: 'אמריקאי', value: 'multiple' }, { text: 'נכון לא נכון', value: 'boolean' }]
const categories: option[] = [
    { text: 'מידע כללי', value: '9' }, 
    { text: 'ספרים', value: '10' }, 
    { text: 'גדצטים', value: '30' }, 
    { text: 'גיאוגרפיה', value: '22' }
]

export { difficulties, types, categories };