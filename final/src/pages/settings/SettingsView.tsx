import { useState, ChangeEvent, FC } from "react";
import { Link } from "react-router-dom";
import { props } from "./types";
import Select from "../../components/select";
import Input from "../../components/input";
import { difficulties, types, categories } from "./constants";
import loader from "../../assets/loader.svg";
import SettingsStyle from "./Settings.module.css";

const SettingsView: FC<props> = ({
  getQuestions,
  setTimer,
  isQuestionsLoading,
}) => {
  console.log("<Settings View>");

  const [questionAmount, setQuestionAmount] = useState(0);
  const [chosenCategory, setChosenCategory] = useState("");
  const [chosenDifficulty, setChosenCifficulty] = useState("");
  const [gameType, setGameType] = useState("");
  const [chosenTime, setChosenTime] = useState(0);

  const submit = () => {
    getQuestions(questionAmount, chosenCategory, chosenDifficulty, gameType);
    setTimer(chosenTime);
  };

  const changeQuestionAmount = (event: ChangeEvent<HTMLInputElement>) => {
    setQuestionAmount(() => Number(event.target.value));
  };

  const setNewTime = (event: ChangeEvent<HTMLInputElement>) => {
    setChosenTime(() => parseInt(event.target.value));
  };

  const setNewCategory = (event: ChangeEvent<HTMLSelectElement>) => {
    setChosenCategory(() => event.target.value);
  };

  const setNewDifficulty = (event: ChangeEvent<HTMLSelectElement>) => {
    setChosenCifficulty(() => event.target.value);
  };

  const setNewType = (event: ChangeEvent<HTMLSelectElement>) => {
    setGameType(() => event.target.value);
  };

  return (
    <div>
      <Link
        to={"/"}
        className={`button ${SettingsStyle["correct-answer"]} text-black ${SettingsStyle["back-to-home-button"]}`}
      >
        חזרה למסך בית
      </Link>
      <h1 className="text-center text-white">מסך הגדרות</h1>

      <div className={`${SettingsStyle.form} mt-5`}>
        <div className="mr-3">
          <Input
            title="כמות השאלות"
            onChange={changeQuestionAmount}
            type="number"
            textHolder="בחר מספר בין 1 ל50"
            rules={(value) =>
              value ? Number(value) > 0 && Number(value) <= 50 : false
            }
            errorMessage="חייב להיות בין 1 ל50"
          />
        </div>

        <div className="ml-3">
          <Select
            title="הקטגוריה של המשחק"
            textHolder="בחר קטגוריה"
            items={categories}
            onChange={setNewCategory}
            errorMessage="חייב לבחור קטגוריה"
            rules={(value) => {
              return Boolean(value);
            }}
          />
        </div>
      </div>

      <div className={`${SettingsStyle.form}`}>
        <div className="mr-3">
          <Input
            title="זמן שיהיה לכל שאלה"
            onChange={setNewTime}
            type="number"
            textHolder="בחר זמן לכל חידה"
            rules={(value) =>
              value ? Number(value) > 20 && Number(value) <= 120 : false
            }
            errorMessage="חייב להיות בין 20 ל120 שניות"
          />
        </div>
        <div className="ml-3">
          <Select
            title="רמת הקושי"
            textHolder="בחר רמת קושי"
            items={difficulties}
            onChange={setNewDifficulty}
            errorMessage="חייב לבחור רמת קושי"
            rules={(value) => Boolean(value)}
          />
        </div>
      </div>

      <div className={`${SettingsStyle.form}`}>
        <Select
          title="סוג המשחק"
          textHolder="בחר סוג משחק"
          items={types}
          onChange={setNewType}
          errorMessage="חייב לבחור סוג"
          rules={(value) => Boolean(value)}
        />
      </div>

      <div className={`${SettingsStyle.form} mt-4`}>
        <button className="button blue-background" onClick={submit}>
          {!isQuestionsLoading ? (
            "התחל משחק"
          ) : (
            <img src={loader} className={`${SettingsStyle.spinner}`} />
          )}
        </button>
      </div>
    </div>
  );
};

export default SettingsView;
