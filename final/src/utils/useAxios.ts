import { useState } from "react";

const useAxios = <T>() => {
  const [data, setData] = useState<T | null>(null);
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const [error, setError] = useState<Error | null>(null);

  const getAxios = async (
    apiFunction: (...args: any[]) => Promise<T>,
    ...args: any[]
  ) => {
    setIsLoading(() => true);
    try {
      const response = await apiFunction(...args);
      setData(() => response);
    } catch (error) {
      if (error instanceof Error) {
        setError(error);
      } else {
        setError(Error("no data"));
      }
    }

    setIsLoading(() => false);
  };

  return { data, isLoading, error, getAxios };
};

export { useAxios };
