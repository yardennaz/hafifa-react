import { Routes, Route, useNavigate } from "react-router-dom";
import { useState } from "react";
import { answer, question } from "./types";
import Swal from "sweetalert2";
import OpeningView from "./pages/opening/";
import GameView from "./pages/game/";
import SettingsView from "./pages/settings/";
import SummaryView from "./pages/summary/";
import api from "./api/api";
import { QuizContextProvider } from "./store/gameStore";
import { useAxios } from "./utils/useAxios";
import "./index.css";

const App = () => {
  const navigate = useNavigate();
  const [answers, setAnswers] = useState<answer[]>([]);
  const [timer, setTimer] = useState<number>(0);

  const {
    data: questions,
    isLoading: isQuestionsLoading,
    error,
    getAxios,
  } = useAxios<question[]>();

  const setNewTime = (newTime: number) => {
    setTimer(() => newTime);
  };

  const resetAnswers = () => {
    setAnswers(() => []);
  };

  const getQuestion = async (
    amount: number,
    category: string,
    difficulty: string,
    type: string
  ) => {
    try {
      await getAxios(api.quiz.getAll, amount, category, difficulty, type);

      if (questions && questions.length === 0) {
        throw Error("תוודא שכל הפרמטרים מוגדרים");
      }

      if (error) {
        throw error;
      }

      navigate("/game");
    } catch (error) {
      if (error instanceof Error) {
        Swal.fire({
          title: "שגיאה",
          text: error.message,
          icon: "error",
          confirmButtonText: "בסדר",
        });
      }
    }
  };

  const pushNewAnswer = (newAnswer: answer) => {
    setAnswers((prevState) => {
      return [...prevState, newAnswer];
    });
  };

  return (
    <QuizContextProvider>
      <Routes>
        <Route
          path="/"
          element={
            <OpeningView
              isQuestionsLoading={isQuestionsLoading}
              getQuestions={getQuestion}
              setTimer={setNewTime}
            />
          }
        />
        <Route
          path="settings"
          element={
            <SettingsView
              getQuestions={getQuestion}
              setTimer={setNewTime}
              isQuestionsLoading={isQuestionsLoading}
            />
          }
        />
        <Route
          path="game"
          element={
            <GameView
              pushNewAnswer={pushNewAnswer}
              answers={answers}
              timer={timer}
              questions={questions ? questions : []}
            />
          }
        />
        <Route
          path="summary"
          element={
            <SummaryView answers={answers} resetAnswers={resetAnswers} />
          }
        />
      </Routes>
    </QuizContextProvider>
  );
};

export default App;
