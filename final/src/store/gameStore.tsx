import { FC, createContext, useState, ReactNode } from "react";

const QuizContext = createContext<context>({
  currentQuestion: 0,
  changeQuestion: () => {},
  points: 0,
  setNewPoints: () => {},
  emptyPoints: () => {},
});

type store = {
  children: ReactNode;
};

type context = {
  currentQuestion: number;
  changeQuestion: (index: number) => void;
  points: number;
  setNewPoints: (newPoints: number) => void;
  emptyPoints: () => void;
};

const QuizContextProvider: FC<store> = ({ children }) => {
  const [currentQuestion, setCurrentQuestion] = useState<number>(0);
  const [points, setPoints] = useState<number>(0);

  const changeQuestion = (newIndex: number) => {
    setCurrentQuestion(() => newIndex);
  };

  const setNewPoints = (newPoints: number) => {
    setPoints((oldPoints) => oldPoints + newPoints);
  };

  const emptyPoints = () => {
    setPoints(() => 0);
  };

  return (
    <QuizContext.Provider
      value={{
        currentQuestion,
        changeQuestion,
        points,
        setNewPoints,
        emptyPoints,
      }}
    >
      {children}
    </QuizContext.Provider>
  );
};

export { QuizContext, QuizContextProvider };
