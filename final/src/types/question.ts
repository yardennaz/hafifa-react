export type question = {
    type: 'multiple' | 'boolean',
    difficulty: 'hard' | 'medium' | 'easy',
    category: string,
    question: string,
    correctAnswer: string,
    incorrectAnswers: string[],
}

