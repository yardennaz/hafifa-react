export type rawQuestion = {
    type: 'multiple' | 'boolean',
    difficulty: 'hard' | 'medium' | 'easy',
    category: string,
    question: string,
    correct_answer: string,
    incorrect_answers: string[],
}