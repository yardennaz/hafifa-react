type answer = {
    userAnswer: string,
    correctAnswer: string,
    question: string,
}

export type { answer }