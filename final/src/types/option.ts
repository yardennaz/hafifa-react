type option = {
    text: string,
    value: string | number
}

export type { option };