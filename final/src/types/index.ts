import { option } from "./option";
import { answer } from "./answer";
import { rawQuestion } from "./rawQuestions";
import { question } from "./question";

export type { option, answer, question, rawQuestion };