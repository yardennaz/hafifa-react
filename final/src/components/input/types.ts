import { ChangeEvent, HTMLInputTypeAttribute } from 'react';

type props = {
    onChange: (event: ChangeEvent<HTMLInputElement>) => void,
    rules?: (value: string | undefined) => boolean,
    textHolder: string | undefined,
    type: HTMLInputTypeAttribute | undefined,
    errorMessage: string,
    title: string,
}

export default props;