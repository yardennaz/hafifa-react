import { FC, useRef, ChangeEvent } from "react";
import props from "./types";
import InputStyle from "./Input.module.css";

const Select: FC<props> = ({
  textHolder,
  type,
  rules,
  errorMessage,
  onChange,
  title,
}) => {
  const ref = useRef<HTMLInputElement>(null);

  const change = (event: ChangeEvent<HTMLInputElement>) => {
    onChange(event);
  };

  return (
    <div className={`${InputStyle.column}, text-start`}>
      {title && <h4 className={`text-white`}>:{title}</h4>}
      <input
        ref={ref}
        className={`${InputStyle.input}`}
        placeholder={textHolder}
        type={type}
        onChange={change}
      />
      <>
        {rules && !rules(ref ? ref.current?.value : "") && (
          <div className={`${InputStyle.error}`}> {errorMessage} </div>
        )}
      </>
    </div>
  );
};

export default Select;
