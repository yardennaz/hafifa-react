import React from 'react';
import { Fireworks } from '@fireworks-js/react';

const MyFireworksComponent: React.FC = () => {
  return (
    <div style={{ position: 'fixed', width: '100%', height: '100vh' }}>
      <Fireworks />
    </div>
  );
};

export default MyFireworksComponent;