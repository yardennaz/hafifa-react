import { useEffect, useState, FC } from "react"
import { props } from "./types";
import TimerStyle from './Timer.module.css';

const QuestionTimer: FC<props> = ({time, onTimeout}) => {
    const TIME_INTERVAL = 50;

    const [remainingTime, setRemainingTime] = useState(time);

    useEffect(() => {
        const timer = setTimeout(() => {
            onTimeout()
        }, time);
        return () => {
            clearTimeout(timer)
        }
    }, [onTimeout, time])

    useEffect(() => {
        const interval = setInterval(() => {
            setRemainingTime((oldTime) => oldTime - TIME_INTERVAL);
        }, TIME_INTERVAL)

        return () => { clearInterval(interval) }
    }, [])
    
    return <progress className={`${TimerStyle.progress}`} max={ time } value={ remainingTime }/>
}

export default QuestionTimer;