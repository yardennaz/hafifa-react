export type props = {
    time: number,
    onTimeout: () => void,
}