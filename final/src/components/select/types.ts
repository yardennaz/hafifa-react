import { ChangeEvent } from "react";
import { option } from "../../types";

type props = {
  onChange: (event: ChangeEvent<HTMLSelectElement>) => void;
  rules?: (value: string | undefined) => boolean;
  items: option[];
  textHolder: string;
  errorMessage: string;
  title: string;
};

export default props;
