import { FC, useRef, ChangeEvent } from "react";
import props from "./types";
import SelectStyle from "./Select.module.css";

const Select: FC<props> = ({
  items,
  textHolder,
  rules,
  errorMessage,
  onChange,
  title,
}) => {
  const ref = useRef<HTMLSelectElement>(null);

  const change = (event: ChangeEvent<HTMLSelectElement>) => {
      onChange(event);
  };

  return (
    <div className="text-start">
      {title && <h4 className={`text-white`}>:{title}</h4>}

      <select
        ref={ref}
        value={ref.current?.value}
        className={`${SelectStyle.select}`}
        onChange={change}
      >
        <option value="" hidden>
          {textHolder}
        </option>
        {items.map((item) => (
          <option key={item.text} value={item.value}>
            {item.text}
          </option>
        ))}
      </select>
      <>
        {rules && !rules(ref.current?.value) && (
          <div className={`${SelectStyle.error} text-start`}>
            {errorMessage}
          </div>
        )}
      </>
    </div>
  );
};

export default Select;
