import axios from "axios";
import { question } from "../types";
import { rawQuestion } from "../types";

const instance = axios.create({ baseURL: "https://opentdb.com/api.php" });

export default {
  quiz: {
    getAll: async (
      amount: number,
      category: string,
      difficulty: string,
      type: string
    ): Promise<question[]> => {
      const { data } = await instance.get(
        `?amount=${amount}&category=${category}&difficulty=${difficulty}&type=${type}`
      );

      return data.results.map((question: rawQuestion) => ({
        ...question,
        incorrect_answers: undefined,
        incorrectAnswers: question.incorrect_answers,
        correct_answer: undefined,
        correctAnswer: question.correct_answer,
      }));
    },
  },
};
